package airline.delay;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import airline.util.AirlineInfoParser;
/*
 * 입력 :  라인번호             -  항공운항데이
 * 출력 :  운항연도,운항월 -  출발 지연 건수
 */
public class DepartureDelayMapper extends Mapper<LongWritable, Text, Text, IntWritable>{

	IntWritable outputValue = new IntWritable(1);
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
		
//		AirParser를 이용해 도착지연시간이 0보다 큰 값인지 확인한다.
//		true일 경우 운항연도.운항월 - 건수1  를 key - value로 설정해 출력
		AirlineInfoParser parser = new AirlineInfoParser(value);
		if (parser.getDepartureDelayTime() > 0) {
			Text outputKey = new Text(parser.getYear()+"/"+parser.getMonth());
			context.write(outputKey, outputValue);
		}
	

	}
	
}
