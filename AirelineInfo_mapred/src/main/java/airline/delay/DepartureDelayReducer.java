package airline.delay;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
/*
 * 입력 : 운항연도,운항월 -  출발 지연 건수
 * 출력 : 운항연도,운항월 -  출발 지연 건수 합계
 */
public class DepartureDelayReducer extends Reducer<Text, IntWritable, Text, IntWritable>{
	
	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
//		운항연도,운항월 -  출발 지연 건수 합계를 계산해 결과로 출력
		int sum = 0;
		for(IntWritable value : values) {
			sum = sum + value.get();
		}
		context.write(key, new IntWritable(sum));
	}
	
}
