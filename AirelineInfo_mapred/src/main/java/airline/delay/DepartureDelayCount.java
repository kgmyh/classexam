package airline.delay;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


/*
 *                              키                  -          값  
 * 매퍼    입력 :  라인번호             -  항공운항데이
 *           출력 : 운항연도,운항월    -  출발 지연 건수
              
 * 리듀서 입력 : 운항연도,운항월 -  출발 지연 건수
 *           출력 : 운항연도,운항월 -  출발 지연 건수 합계
*/
public class DepartureDelayCount {

	public static void main(String[] args) throws Exception{
		
		if(args.length != 2 ){
			System.err.println("Usage : DepartureDelayCount <input> <output>");
			System.exit(1);
		}
		Configuration conf = new Configuration();
		//Job 객체 생성 
		Job job = Job.getInstance(conf);
		//Runner 클래스 설정.
		job.setJarByClass(DepartureDelayCount.class);
		//입력 File 경로 설정. 
		FileInputFormat.addInputPath(job, new Path(args[0]));
		//출력 경로 설정.
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		//Mapper와 Reducer class 설정.
		job.setMapperClass(DepartureDelayMapper.class);
		job.setReducerClass(DepartureDelayReducer.class);
		//입출력 데이터 포멧 설정.
		//Raw 데이터 입력 포멧 설정 - 라인 - Text 형태로 읽을 것이므로 TextIputFormat 설정
		job.setInputFormatClass(TextInputFormat.class);
		//리듀서가 최종 결과를 출력할 포멧 설정
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//리듀서의 출력(output) 키와 값의 타입 설정
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		//JobTracker에게 처리 요청.
		job.waitForCompletion(true);
	}	
}