package airline.util;

import org.apache.hadoop.io.Text;

/*
 * 한줄의 운항정보를 받아서 , 구분자를 기준으로 데이터를 잘라 instance변수에 할당.
 * 출발/도착 지연시간은 정수형이며 데이터에는 NA값이 있기 때문에 NA일 경우는 값을 그대로 놓고 NA인지 확인하는 boolean 변수를 사용.
 *
 * 도착, 출발 지연만 확인 할 것이므로 그와 관련된 데이터를 처리하지만 원래는 다 처리해야 한다. 
 */
public class AirlineInfoParser {

	private String year;
	private String month;
	
	private int departureDelayTime;
	private int arriveDelayTime;
	private int distance;
	
	private String uniqueCarrier;
	
	public AirlineInfoParser(Text value) {
		String [] columns = value.toString().split(",");
		//운항 년도
		year = columns[0];
		//운항 월
		month = columns[1];
		
		//항공사 코드.
		uniqueCarrier = columns[8];
		
		//항공기 도착지연시간 설정 - NA처리 
		if(!columns[14].equals("NA")) {
			arriveDelayTime = Integer.parseInt(columns[14]);
		}
		
		//항공기 출발 지연시간 설정 - NA가 있으므로 처리.
		if(!columns[15].equals("NA")) {
			departureDelayTime = Integer.parseInt(columns[15]);
		}
		//운항거리 설정 - NA처리
		if(!columns[18].equals("NA")) {
			distance = Integer.parseInt(columns[18]);
		}
	}

	public String getYear() {
		return year;
	}

	public String getMonth() {
		return month;
	}

	public int getDepartureDelayTime() {
		return departureDelayTime;
	}

	public int getArriveDelayTime() {
		return arriveDelayTime;
	}

	public int getDistance() {
		return distance;
	}

	public String getUniqueCarrier() {
		return uniqueCarrier;
	}

	
	
}
