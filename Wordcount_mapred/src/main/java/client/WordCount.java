package client;
import mapper.TokenizerMapper;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import reducer.IntSumReducer;

//Runner class
public class WordCount {
	
	public static void main(String[] args) throws Exception {
		
        if(args.length != 2) {
            System.err.println("Usage : WordCount <input path> <output path>");
            System.exit(1);
        }

//      Configuration  설정 - 설정정보를 가지고 있는 클래스

//      Job 생성
        
//      Runner(Job 생성,설정 및 맵리듀스 실행 클래스) 설정        

//		매퍼 설정
        
//		컴바이너 클래스 설정
		
//		리듀서 설정
        

//		리듀서의 처리결과 key 타입 설정

//		리듀서의 처리결과 값(value) 타입 설정

//		맵에게 전달될 데이터 포멧 설정

//		리듀서가 처리결과를 출력하는 포멧 설정

		
//		처리할 파일의 경로

//		처리결과를 출력할 경로 - 이미 존재하는 경로일 경우 에러가 난다.

//		MapReduce  요청

	}
}
